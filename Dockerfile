FROM python:3.6
 ENV PYTHONUNBUFFERED 1
 RUN mkdir /code
 WORKDIR /code
 ADD requirements.txt /code/
 RUN pip install -r requirements.txt
 RUN mkdir /root/nltk_data
 ADD ./nltk_data /root/nltk_data
 ADD . /code/
 
