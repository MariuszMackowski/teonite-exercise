from bs4 import BeautifulSoup
import requests
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import nltk
import re
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

class Author:
    def __init__(self, name, dic):
        self.name = name
        self.words = dic

    def update_dict(self, dic):

        dd = self.words

        for key, v in dic.items():
            if key in dd.keys():
                i = dd[key]
                del dd[key]
                dd[key] = v + i

            else:
                dd[key] = v

        self.words = dd

    def sort(self):
        self.words = sorted(self.words.items(), key=lambda kv: kv[1])[::-1]


def find_author(page):
    for span in page.find_all("span"):

        content = str(span.contents)
        # print(content)
        pattern = re.compile("<h4>.*</h4>")
        find = pattern.findall(content)
        if len(find) > 0:
            return find[0][4:-5:]


def get_page(url):
    r = requests.get(url)
    data = r.text
    return BeautifulSoup(data, 'html.parser')


def word_count(text):
    counts = dict()
    words = text.split()
    stop_words_eng = set(stopwords.words('english'))
    stop_words_pl = set(stopwords.words('polish'))

    for word in words:
        if word not in stop_words_eng and word not in stop_words_pl:
            if word in counts:
                counts[word] += 1
            else:
                counts[word] = 1

    return counts


def get_words(page):
    text = []
    for paragraph in page.find_all("p"):
        text.append(paragraph.text)
    return word_count("".join(text).lower())


def get_links(url):
    bs = get_page(url)
    links = []
    for link in bs.find_all("a"):
        match = re.search("^/blog/[^/]*/$", str(link.get("href")))
        if match:
            links.append(str('https://teonite.com' + link.get("href")))
    return links[::2]


def update_authors():
    urls = ['https://teonite.com/blog/', "https://teonite.com/blog/page/2/", "https://teonite.com/blog/page/3/",
            "https://teonite.com/blog/page/4/"]
    authors = dict()
    authors["all"] = Author("All", dict())
    alll = authors["all"]
    for url in urls:
        for link in get_links(url):
            author_name = find_author(get_page(link))
            words = get_words(get_page(link))
            alll.update_dict(words)
            if author_name not in authors.keys():
                authors[author_name] = Author(author_name, words)
            else:
                author = authors[author_name]
                author.update_dict(words)

    return authors


def prepare_data(author):
    author.sort()
    important_words = list(author.words[:10])

    text = ""

    for i in range(10):
        text += important_words[i][0]+","+str(important_words[i][1])+"|"
    return text[:-1:]


def update_db(authors):

    insert = """INSERT INTO authors (author_name, author_words)
                VALUES (%s, %s)
                """
    create = (
        """
        CREATE TABLE authors (
        author_id SERIAL PRIMARY KEY,
        author_name CHAR(21) NOT NULL,
        author_words TEXT NOT NULL
        )
        """
    )
    drop = (
        """
        DROP TABLE authors
        """)
    conn = None    
    try:
        # read the connection parameters

        # connect to the PostgreSQL server
        conn = psycopg2.connect("dbname=teonite user=postgres password=postgres host=dck_db_1")
        
        cur = conn.cursor()
        
        cur.execute(create)
        # create table one by one
        for k,v in authors.items():
            name = v.name
            words = prepare_data(v)
            cur.execute(insert, (name, words))
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def get_authors_from_db():

    conn = None
    try:
        conn = psycopg2.connect("dbname=teonite user=postgres password=postgres host=dck_db_1")
        cur = conn.cursor()
        cur.execute("SELECT author_name, author_words FROM authors")
        row = cur.fetchone()
        authors = dict()
        while row is not None:            
            authors[row[0]] = row[1]
            row = cur.fetchone()
        cur.close()
        return authors
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def split_text(text_from_db):
    spl = text_from_db.split("|")
    words = dict()
    for word in spl:
        temp = word.split(",")
        words[temp[0]] = int(temp[1])
    return words


def create_db():
    con = psycopg2.connect(user='postgres', host='dck_db_1',
          password='postgres')

    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    cur = con.cursor()
    cur.execute("CREATE DATABASE teonite;")

