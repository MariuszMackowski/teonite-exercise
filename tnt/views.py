from django.http import HttpResponse
import psycopg2
from .fx import update_authors, update_db, get_authors_from_db, split_text, create_db
import json


def index(request):
    try:
        create_db()
    except (Exception) as error:
        print(error)
        return HttpResponse("Db already created or error")
    else:
        return HttpResponse("Hello, world. Db created.")


def stats(request):
    authors = get_authors_from_db()
    string = ""
    for k,v in authors.items():
        string += v
        break
    string = split_text(string)
    return HttpResponse(json.dumps(string), content_type='application/json')

def author(request, author):
    all = get_authors_from_db()
    dic = dict()
    for k,v in all.items():
        name = k.split(" ")
        name = (name[0] + name[1]).lower()
        dic[name]= v
    del dic["all"]
    try:
        data = split_text(dic[author])
    except (Exception, KeyError) as error:
        print(error)
        return HttpResponse("Author not found")
    else:	
        return HttpResponse(json.dumps(data, ensure_ascii=False).encode('utf8'))

def authors(request):
    all = get_authors_from_db()
    dic = dict()
    for k,v in all.items():
        name = k.split(" ")
        name = (name[0] + name[1]).lower()
        dic[name]= k
    del dic["all"] 
    return HttpResponse(json.dumps(dic, ensure_ascii=False).encode('utf8'))


def update(request):
    update_db(update_authors())
    return HttpResponse("Hello, world. updated")